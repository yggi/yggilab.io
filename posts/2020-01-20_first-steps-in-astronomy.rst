.. title: First Steps in Astronomy
.. slug: first-steps-in-astronomy
.. date: 2020-01-20 16:01:02 UTC+01:00
.. tags: astronomy, photography
.. category: 
.. link: 
.. description: 
.. type: text


During the summer of 2019, I took some first steps into Astronomy and Astrophotography. After discovering an ancient Novoflex Noflexar 400mm lens at a flea-market, and designing and printing an extension tube to connect it to my Olympus E-M10mkII Camera, I had a nice hacked-together setup to take some pictures of big things far away.

.. thumbnail:: /images/2019-08-15_noflexar_setup.jpg
   :alt: Novoflex Noflexar 
   
   Novoflex Noflexar 400mm mounted with Olympus E-M10mkII

First target was the moon. This was actually a hand-held shot, the moon is quite bright and the image stabilization of the camera helps a lot.

.. thumbnail:: /images/2019-08-10T21-43-11_moon.jpg
   :alt: Moon 
   
   The moon (400mm 1/320s F5.6)

After that, I went for the big planets. For those pictures, the setup was mounted on a travel tripod, which was extended with scrap wood.

I started with Saturn. Of course, there are by far better pictures of it, but for noob-me with some improvised equipment, it was very satisfiying to actually see the rings with my own eyes and camera

.. thumbnail:: /images/2019-08-12T23-06-27_saturn.jpg
   :alt: Saturn
   
   Saturn with rings

Next was Jupiter. This took some postprocessing in darktable and gimp, but I was able to capture the four Galilean moons. The reference chart was generated with the great tool at http://shallowsky.com/jupiter/. Unfortunately, I was not able to extract any structure of Jupiter itself from the pictures.

.. thumbnail:: /images/2019-08-12T23-12-47_58_jupiter_moons_annotated.jpg
   :alt: Jupiter
   
   Jupiter with Galilean Moons
